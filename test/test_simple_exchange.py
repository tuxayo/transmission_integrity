import unittest
import threading
import random
import os
import traceback

import transmission_integrity.client as cli
import transmission_integrity.server as srv


class Test(unittest.TestCase):
    def test_simple_exchange(self):
        server_port = random.randint(1024, 64000)
        server_thread = GenericThread(server, server_port)
        client_thread = GenericThread(client, server_port)
        client_thread.start()
        server_thread.start()
        client_thread.join()
        server_thread.join()


def client(server_port):
    socket = cli.connect_to_server(port=server_port)
    cli.send_message(socket, 'send 42 from Alice to Bob')
    socket.close()


def server(port):
    listen_sock = srv.listen(port=port)
    client_sock = srv.wait_for_client(listen_sock)
    srv.handle_wire_transfert(client_sock)
    client_sock.close()
    listen_sock.close()


class GenericThread(threading.Thread):
    def __init__(self, function, *args):
        super().__init__()
        self.function = function
        self.args = args

    def run(self):
        try:
            self.function(*self.args)
        except:
            # Exceptions in threads are not propagated to caller which excepted
            # this is the simplest way to make the process fail.
            # However it terminates even the unittest runner so no other tests
            # are ran and there is no report for the test suite
            # See https://git.framasoft.org/tuxayo/transmission_integrity/issues/2
            traceback.print_exc()
            os._exit(1)

if __name__ == "__main__":
    unittest.main()
