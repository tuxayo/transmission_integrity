import hmac
import random


def make_mac(key, message_str):
    message_bytes = message_str.encode('utf-8')
    return hmac.new(key, message_bytes, 'SHA256').hexdigest()


def generate_nonce():
    """
    Based on python-oauth2 implementation
    https://github.com/joestump/python-oauth2/blob/007ac36ac8b1e7a9c5ab868f1b3fc1eb301c22a9/oauth2/__init__.py#L171
    """
    length = 8
    return ''.join([str(random.SystemRandom().randint(0, 9))
                    for i in range(length)])
