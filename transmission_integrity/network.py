def send_in_socket(message_str, sock):
    message = message_str.encode('utf-8')
    sock.sendall(message)
