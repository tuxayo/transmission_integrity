import socket
import json

import transmission_integrity.crypto as crypto
import transmission_integrity.network as network


HOST = ''  # Symbolic name meaning all available interfaces
KEY = b'TODO get me from config file'


def listen(port=7070):
    listen_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    listen_sock.bind((HOST, port))
    listen_sock.listen(1)  # listen with a backlog of 1
    return listen_sock


def wait_for_client(listen_sock):
    print('SERVER: Waiting for client')
    client_sock, client_adress = listen_sock.accept()
    print('SERVER: Connected by', client_adress)
    return client_sock


def handle_wire_transfert(conn_sock):
    nonce = crypto.generate_nonce()
    network.send_in_socket(nonce, conn_sock)

    raw_message = conn_sock.recv(1024).decode("utf-8")
    message = _extract_message_and_check_integrity(raw_message, nonce)
    if message == 'INTEGRITY_ERROR':
        print('SERVER: Integrity issue detected')
        conn_sock.sendall(b'INTEGRITY_ERROR')
    else:
        conn_sock.sendall(b'ACK')
        print('SERVER: Processing wire transfert:', message)

    conn_sock.close()


def _extract_message_and_check_integrity(raw_message_with_metadata,
                                         server_nonce):
    try:
        message_with_metadata = json.loads(raw_message_with_metadata)
        message_with_nonce = json.loads(message_with_metadata['message_with_nonce'])
    except json.decoder.JSONDecodeError:
        print('SERVER: ERROR: Data received is not valid JSON')
        return 'INTEGRITY_ERROR'

    # check MAC
    mac = crypto.make_mac(KEY, message_with_metadata['message_with_nonce'])
    if mac != message_with_metadata['mac']:
        print("SERVER: ERROR: MAC doesn't match")
        return 'INTEGRITY_ERROR'

    # check nonce
    if server_nonce != message_with_nonce['nonce']:
        print("SERVER: ERROR: nonce doesn't match")
        return 'INTEGRITY_ERROR'

    # all good!
    return message_with_nonce['message']
