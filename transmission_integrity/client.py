import socket
import time
import json

import transmission_integrity.crypto as crypto
import transmission_integrity.network as network

KEY = b'TODO get me from config file'


def connect_to_server(host='localhost', port=7070):
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    _connect_with_retry(sock, host, port)
    return sock


def send_message(sock, message):
    nonce = _get_nonce(sock)
    message_with_nonce = json.dumps({'message': message, 'nonce': nonce})
    mac = crypto.make_mac(KEY, message_with_nonce)
    message_with_mac_and_nonce = json.dumps(
        {'message_with_nonce': message_with_nonce,
         'mac': mac})
    _send_data(sock, message_with_mac_and_nonce)


def _get_nonce(sock):
    return sock.recv(1024).decode("utf-8")


def _send_data(sock, message):
    network.send_in_socket(message, sock)
    data = sock.recv(1024)
    sock.close()
    print('CLIENT: server responded: ', data.decode('utf-8'))


def _connect_with_retry(socket, host, port):
    try:
        socket.connect((host, port))
    except ConnectionRefusedError:
        print('server not found, retrying')
        time.sleep(1)
        _connect_with_retry(socket, host, port)
